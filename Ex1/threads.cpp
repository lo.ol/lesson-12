#include "threads.h"

mutex _mt;

/*gets all prime numbers between begin and end if file is open and prints them to given file*/
void writePrimesToFile(int begin, int end, ofstream & file)
{
	
	if (file.is_open())
	{
		bool flag = true;
		for (int i = begin; i <= end; i++)
		{
			for (int j = 2; j <= sqrt(i) && flag; j++)
			{
				if (i % j == 0)
				{
					flag = false;
				}
			}
			
			if (flag)
			{
				lock_guard<mutex> lck(_mt);
				file << i << "\n";
			}
			flag = true;
		}

	}
	else
	{
		cout << "Error! file did not open!";
	}
}


void callWritePrimesMultipleThreads(int begin, int end, string filePath, const int N)
{
	int newend, diff = (end - begin) / N; //divides numbers to check to N threades 
	vector<thread> threads;
	ofstream file;

	file.open(filePath);

	clock_t tStart = clock();
	for (int i = begin; i <= end; i += diff + 1) //create N threades
	{
		newend = i + diff;
		if (newend > end)
		{
			newend = end;

		}

		threads.push_back(thread(writePrimesToFile, ref(i), ref(newend), ref(file)));


	}

	for (int i = 0; i < N; i++)
	{
		threads[i].join();
	}
	cout << "time is: " << (double)(clock() - tStart) / CLOCKS_PER_SEC << endl; //prints time that took to thread
	file.close();

}

