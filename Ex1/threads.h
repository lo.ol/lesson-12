#pragma once
#include <math.h>
#include <string>
#include <fstream>
#include <time.h>
#include <iostream>
#include<vector>
#include <thread>
#include <mutex>

using namespace std;



void writePrimesToFile(int begin, int end, ofstream& file);
void callWritePrimesMultipleThreads(int begin, int end, string filePath, const int N);
