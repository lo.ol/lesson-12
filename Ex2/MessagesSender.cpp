#include "MessagesSender.h"



MessagesSender::MessagesSender()
{
}


MessagesSender::~MessagesSender()
{
}

/*prints main*/
void MessagesSender::printMenu()
{
	cout << "1.	Signin\n2.	Signout\n3.	Connected Users\n4.	exit\n";
}

/*function gets a name to add as a user if user does not exist yet*/
void MessagesSender::signin()
{
	string userName;
	cout << "Please enter user name: " << endl;
	cin >> userName;
	unique_lock<mutex> lock(_mtx);
	if (_user.find(userName) != _user.end())
	{
		cout << "There is a user by this name!" << endl;
	}
	else
	{
		_user.insert(userName);
	}
	lock.unlock();
}

/*function gets a name to erase from user set if user exists */
void MessagesSender::signout()
{
	string userName;
	cout << "Please enter user name: " << endl;
	cin >> userName;
	unique_lock<mutex> lock(_mtx);
	if (_user.find(userName) == _user.end())
	{
		cout << "There is no user by this name!!" << endl;
	}
	else
	{
		_user.erase(userName);
	}
	lock.unlock();
}

/*function prints all connected users*/
void MessagesSender::connectedUsers()
{
	set<string>::iterator it;
	cout << "Connected users: " << endl;
	for (it = _user.begin(); it != _user.end(); it++)
	{
		cout << *it << endl;
	}
}

/*function gets data from file "data.txt"
gets line by line strings and puts them in a queue of data strings*/
void MessagesSender::readData()
{
	fstream file;
	string line;
	
	while (1)
	{
		
		
		file.open("data.txt");
		if (file.is_open())
		{
			unique_lock<mutex> lock(_mtx);
			while (getline(file, line))
			{
				_data.push(line);
			}
			lock.unlock();
			_cond.notify_one();
			file.close();
			file.open("data.txt", ifstream::out | ifstream::trunc);
			file.close();
		}
		
		
		this_thread::sleep_for(chrono::seconds(60));
		

	}
	
}
/*function prints data from queue of data to "output.txt"
file with users name(if users exist)*/
void MessagesSender::spreadData()
{
	ofstream outFile;
	set<string>::iterator it;
	outFile.open("output.txt");
	while (1)
	{
		
		
		
		if (outFile.is_open())
		{
			unique_lock<mutex> lock(_mtx);
			_cond.wait(lock, [&]() {return !_data.empty(); });
			if (_user.empty())
			{
				while (!_data.empty()) {

					outFile  << _data.front() << endl;

					_data.pop();
				}
			}
			else {
				while (!_data.empty()) {

					for (it = _user.begin(); it != _user.end(); it++)
					{
						outFile << *it << ":" << _data.front() << endl;

					}
					_data.pop();
				}
			}
			
			lock.unlock();
		}
		
	}
	outFile.close();
}

/*function combines all functions together*/
void MessagesSender::main()
{
	int choice = 0;
	while (choice != EXIT)
	{
		system("cls");
		printMenu();
		cin >> choice;
		switch (choice)
		{
		case SIGNIN:
			signin();
			break;
		case SIGNOUT:
			signout();
			break;
		case CONNUSERS:
			connectedUsers();
			break;
		default:
			break;
		}
		system("pause");
	}
}


