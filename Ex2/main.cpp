#include "MessagesSender.h"

int main(void)
{
	MessagesSender m;
	
	thread read(&MessagesSender::readData, &m);
	thread menu(&MessagesSender::main, &m);
	thread write(&MessagesSender::spreadData, &m);
	write.detach();
	read.detach();
	menu.join();
	
	
	return 0;
}