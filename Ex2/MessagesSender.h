#pragma once
#include <iostream>
#include <fstream>

#include <string>
#include <set>
#include <mutex>
#include <iterator>
#include <queue>
#include <thread>
using namespace std;
enum options{ SIGNIN = 1, SIGNOUT,CONNUSERS,EXIT };
class MessagesSender
{
public:
	MessagesSender();
	~MessagesSender();
	void printMenu();
	void signin();
	void signout();
	void connectedUsers();
	void readData();
	void spreadData();
	void main();

	
private:
	set<string> _user;
	queue<string> _data;
	mutex _mtx;
	condition_variable _cond;
};

